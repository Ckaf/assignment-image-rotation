#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H

#include "image.h"
#include <stddef.h>
/* создаёт копию изображения, которая повёрнута на 90 градусов */

void rotate( struct image const *img, struct image *result );

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
