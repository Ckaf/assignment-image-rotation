#include <stdio.h>

#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_FILE_H

enum open_status {
    OP_S_FILE_NOT_EXIST,
    OP_S_NOT_ENOUGH_RIGHTS,
    OP_S_OK = 0
};

inline FILE * open_file_r(const char *filepath){
    return fopen(filepath, "rb");
}

inline FILE * open_file_w(const char *filepath){
    return fopen(filepath, "w+");
}

enum close_status {
    CL_S_FILE_NOT_EXIST,
    CL_S_OK = 0
};
inline void close_file(FILE *file){
    if (file != NULL)
        fclose(file);
}

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_FILE_H
