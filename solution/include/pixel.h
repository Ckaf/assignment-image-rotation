#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H

#include <stdint.h>
struct __attribute__ ((packed)) pixel
 {
    uint8_t b, g, r;
};

#define PIXEL_SIZE sizeof(struct pixel)

#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
