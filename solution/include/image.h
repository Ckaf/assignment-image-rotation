#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include "pixel.h"
#include <stdlib.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

void create_image(struct image *img, uint64_t width, uint64_t height);

void image_destroy(const struct image *image);

inline struct pixel get_pixel_from_exist(const struct image *image, const uint64_t x, const uint64_t y){
    return image->data[x + y * image->width];
}
inline void set_pixel(struct image *img, const uint64_t x, const uint64_t y, const struct pixel pixel){
    img->data[x + y * img->width] = pixel;
}
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
