#include "../include/io_file.h"
#include "../include/bmp.h"
#include "../include/transformation.h"
#include "image.h"

extern inline FILE * open_file_r(const char *filepath);

extern inline FILE * open_file_w(const char *filepath);

extern inline void close_file(FILE *file);

int main(int argc, char **argv) {

    const char *input_path = argv[1];
    const char *output_path;
    if (argc == 2) {
        output_path = "out.bmp";
    } else {
        output_path = argv[2];
    }
    FILE *input = open_file_r(input_path);
    if (input == NULL) {
        perror("could not read the file");
        return 0;
    }
    struct image img;
    if (from_bmp(input, &img) != READ_OK) {
        perror("READ_ERROR");
        close_file(input);
        return 0;
    }
    close_file(input);


    struct image result;
    rotate(&img, &result);
    image_destroy(&img);

    FILE *out = open_file_w(output_path);
    if (out == NULL) {
        perror("could not write to file");
        return 0;
    }
    if (to_bmp(out, &result) != WRITE_OK) {
        perror("WRITE_ERROR");
    }
    image_destroy(&result);
    close_file(out);

    return 0;
}
