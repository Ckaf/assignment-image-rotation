#include "../../include/image.h"

extern inline struct pixel get_pixel_from_exist(const struct image *image, uint64_t x, uint64_t y);

extern inline void set_pixel(struct image *img, const uint64_t x, const uint64_t y, const struct pixel pixel);

void create_image(struct image *img, const uint64_t width, const uint64_t height) {
    img->height = height;
    img->width = width;
    img->data = malloc(width * height * PIXEL_SIZE);
}

void image_destroy(const struct image *image) {
    if (image != NULL) {
        free(image->data);
    }
}

