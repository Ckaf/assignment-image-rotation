#include "../../include/bmp.h"

#define BMP_TYPE 0x4D42 // идентификатор BMP файла

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum header_status {
    HEADER_ERROR,
    HEADER_OK = 0
};

static enum header_status read_header(FILE *file, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, file)!=1 || header->bfType != BMP_TYPE) {
        return HEADER_ERROR;
    }
    return HEADER_OK;
}

enum image_status {
    IMAGE_ERROR,
    IMAGE_OK = 0
};

static inline uint32_t padding_value(uint32_t width) {
    return (4 - (width * PIXEL_SIZE) % 4) % 4;
}

static enum image_status read_pixels(FILE *file, const struct bmp_header *header, struct image *img) {

    create_image(img, header->biWidth, header->biHeight);

    const uint32_t padding = padding_value(img->width);
    struct pixel *ptr = img->data;
    if (fseek(file, header->bOffBits, SEEK_SET)!=0){
        image_destroy(img);
        return IMAGE_ERROR;
    }

    for (size_t y = 0; y < img->height; ++y) {
        const size_t cs = fread(ptr + img->width * y, PIXEL_SIZE, img->width, file);
        if (cs != img->width) {
            image_destroy(img);
            return IMAGE_ERROR;
        }

        if (padding != 0 && fseek(file, padding, SEEK_CUR) != 0) {
            image_destroy(img);
            return IMAGE_ERROR;
        }
    }


    return IMAGE_OK;
}


enum read_status from_bmp(FILE *file, struct image *img) {
    struct bmp_header header;

    if (read_header(file, &header) != HEADER_OK)
        return READ_INVALID_HEADER;
    if (read_pixels(file, &header, img) == IMAGE_OK) return READ_OK;
    return READ_INVALID_BITS;
}

static struct bmp_header generate_header(uint64_t width, uint64_t height) {
    const int32_t header_size = sizeof(struct bmp_header); //54
    const int32_t image_size = PIXEL_SIZE * width * height;
    return (struct bmp_header) {
            .bfType = BMP_TYPE, // bmp file type
            .bfileSize = header_size + image_size + padding_value(width) * height, //total file size
            .bOffBits = header_size, //the size of the indent to pixels
            .biSize = 40, //the size of the information header, which is 40
            .biWidth = width, //width
            .biHeight = height, //height
            .biPlanes = 1, //the number of color planes for the target device, always equal to 1
            .biBitCount = 8 * PIXEL_SIZE, //how much does a pixel contain
    };
}

static int write_to_file(FILE *out, struct image const *img, struct bmp_header const *header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1) return 1; //write header
    // fwrite return value is a short item count (or zero).


    const size_t padding = padding_value(img->width);
    const char zeroes[] = {0, 0, 0, 0};

    struct pixel *ptr;    //write pixels
    for (size_t y = 0; y < img->height; ++y) {
        ptr = img->data + img->width * y;

        if (fwrite(ptr, PIXEL_SIZE, img->width, out) != img->width)
            return 1;
        if (padding != 0 && fwrite(zeroes, 1, padding, out) != padding)
            return 1;
    }
    return 0;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = generate_header(img->width, img->height);

    if (!write_to_file(out, img, &header))
        return WRITE_OK;
    return WRITE_ERROR;
}


