#include "../../include/transformation.h"

void rotate( struct image const *img, struct image *result ){
    create_image(result,img->height, img->width);

    for (size_t y = 0; y < img->height; y++) {
        for (size_t x = 0; x < img->width; x++) {
            set_pixel(result, (img->height - 1 - y), x , get_pixel_from_exist(img, x, y));
        }
    }
}
